package mams.content;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonContentConverter extends ContentConverter {
    private static ObjectMapper mapper = new ObjectMapper();

    public String type() {
        return "application/json";
    }
    
    public String encode(Class<?> type, Object source) {
        return null;
    }

    public <T> T decode(Class<T> type, String content) {
        try {
            return (T) mapper.readValue(content, type);
        } catch (Exception e) {
            throw new ContentDecodeException("Failed to decode content with type: " + 
                    type.getCanonicalName() + " and content: " + content, e);
        }
    }

}