package mams.passive;

import java.lang.reflect.Field;

import cartago.ARTIFACT_INFO;
import cartago.OPERATION;
import cartago.OUTPORT;
import mams.artifacts.UnknownContentTypeException;
import mams.command.ListItemDeleteFactory;
import mams.command.ListItemPutFactory;
import mams.content.ContentConverter;
import mams.utils.Identifier;
import mams.utils.Utils;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveListItemArtifact extends AbstractPassiveItemArtifact {
    {
        factories.put("DELETE", new ListItemDeleteFactory());
        factories.put("PUT", new ListItemPutFactory());
    }


    @OPERATION public void updateResource(String contentType, String body) throws Exception {
        // Get a converter based on the incoming request content type...
        ContentConverter converter = ContentConverter.getConverter(contentType);
        if (converter == null) throw new UnknownContentTypeException("Unknown Content Type: " + contentType);

        // Convert the content
        Class<?> myClass = Class.forName(className);
        Object data = converter.decode(myClass, body);

        // Check thgat the identifier has not been modified
        Field[] fields = myClass.getFields();
        for (Field field : fields) {
            Object id = getProperty(field.getName());
            if (field.isAnnotationPresent(Identifier.class) && Utils.isInvalidIdentifier(field, data, id)) {
                throw new IllegalArgumentException("Illegal attempt to change the modifier of an existing resource: " + id + " to: " + field.get(data));
            }
        }

        // Update the properties
        for (Field field : fields) {
            if (field.get(data) != null) 
                this.updateObsProperty(field.getName(), field.get(data));
        }
    }    
}
