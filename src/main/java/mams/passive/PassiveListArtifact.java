package mams.passive;

import mams.artifacts.ListArtifact;
import mams.command.PassiveListPostFactory;
import mams.command.PassiveListGetFactory;
import mams.web.Handler;

import cartago.*;



@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveListArtifact extends ListArtifact {
    {
        factories.put("POST", new PassiveListPostFactory());
        factories.put("GET", new PassiveListGetFactory());
    }

    @LINK
	public void attach(String id, Handler childHandler, OpFeedbackParam<String> baseUri) {
        super.attach(id, childHandler, baseUri);
        this.signal("listItemArtifactCreated", id, className);
    }
}


