package mams.passive;

import mams.command.ItemPutFactory;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveItemArtifact extends AbstractPassiveItemArtifact {
    {
        factories.put("PUT", new ItemPutFactory());
    }
}