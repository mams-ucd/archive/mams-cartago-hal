package mams.artifacts;

import java.lang.reflect.Field;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import cartago.OPERATION;
import cartago.OpFeedbackParam;
import cartago.OperationException;
import mams.content.ContentConverter;
import mams.utils.DefaultValue;

public abstract class AbstractItemArtifact extends TypedResourceArtifact {
    protected ObjectMapper mapper = new ObjectMapper();
    
	@OPERATION
	void init(String name, String className) {
        this.name = name;
        this.className = className;
        defineObsProperty("type", className);
         
        try {
            Class<?> myClass = Class.forName(className);
            Field[] fields = myClass.getFields();
            for (Field field : fields) {
                this.defineObsProperty(field.getName(), DefaultValue.forClass(field.getType()));
            }
        } catch (Exception e) {
            System.out.println("Failed to create DataArtifact for: " + className);
            e.printStackTrace();
        }
    }

    @OPERATION
	void init(String name, String className, Object data) {
        this.name = name;
        this.className = className;
        try {
            Class<?> myClass = Class.forName(className);
            Field[] fields = myClass.getFields();
            for (Field field : fields) {
                this.defineObsProperty(field.getName(), field.get(data));
            }
        } catch (Exception e) {
            System.out.println("Failed to create ItemArtifact for: " + className);
            e.printStackTrace();
        }
    }
    
    @OPERATION
    void getStringProperty(String key, OpFeedbackParam<String> value) {
        value.set(this.getObsProperty(key).stringValue());
    }

    @OPERATION
    void setStringProperty(String key, String value) {
        this.updateObsProperty(key, value);
    }

    public Object getObject() {
        try {
            Class<?> myClass = Class.forName(className);
            
            Object object = myClass.newInstance();
            Field[] fields = myClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                field.set(object, this.getObsProperty(field.getName()).getValue());
            }
            return object;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }  
    
    public ObjectNode createItemNode(ResourceArtifact[] children) {
        return createItemNode(this.getObject(), children);
    }    
    
    public ObjectNode createItemNode(Object object, ResourceArtifact[] children) {
        ObjectNode node = mapper.valueToTree(object);
        ObjectNode selfNode = mapper.createObjectNode();
        selfNode.put("href", getUri());
        ObjectNode links = mapper.createObjectNode();
        links.set("self", selfNode);
        for (ResourceArtifact child : children) {
            ObjectNode linkNode = mapper.createObjectNode();
            linkNode.put("href", child.getUri());
            links.set(child.name, linkNode);
        }
        node.set("_links", links);
        return node;
    }

    @OPERATION public void updateResource(String contentType, String body) throws Exception {
        // Get a converter based on the incoming request content type...
        ContentConverter converter = ContentConverter.getConverter(contentType);
        if (converter == null) throw new UnknownContentTypeException("Unknown Content Type: " + contentType);

        // Convert the content
        Class<?> myClass = Class.forName(className);
        Object data = converter.decode(myClass, body);

        // Update the properties
        Field[] fields = myClass.getFields();
        for (Field field : fields) {
            if (field.get(data) != null) 
                this.updateObsProperty(field.getName(), field.get(data));
        }
    }

    @OPERATION void destroyArtifact(){
        try {
			execLinkedOp("out-1", "detach", this.name);
		} catch (OperationException e) {
			e.printStackTrace();
		}
    }
}