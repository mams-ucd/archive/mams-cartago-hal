package mams.artifacts;

public class UnknownContentTypeException extends Exception {

	/**
     *
     */
    private static final long serialVersionUID = -2484369928649626891L;

    public UnknownContentTypeException(String message) {
        super(message);
	}

}
