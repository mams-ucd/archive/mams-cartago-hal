package mams.command;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import mams.artifacts.TypedResourceArtifact;
import mams.active.HTTPRequest;

public class PassiveListGetFactory implements CommandFactory {

    @Override
    public Command create(TypedResourceArtifact artifact, HTTPRequest event) {
        return new PassiveListGetCommand(artifact, event.getCtx(), event.getRequest(), event.getType());
    }

    @Override
    public Command create(TypedResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request) {
        return new PassiveListGetCommand(artifact, ctx, request, artifact.getType());
    }
}