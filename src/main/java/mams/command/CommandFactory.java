package mams.command;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import mams.artifacts.TypedResourceArtifact;
import mams.active.HTTPRequest;

public interface CommandFactory {
    Command create(TypedResourceArtifact artifact, HTTPRequest event);
    Command create(TypedResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request);
}