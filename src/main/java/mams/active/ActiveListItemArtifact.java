package mams.active;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import mams.active.command.ActiveItemRequestNoBodyCommandFactory;
import mams.command.ListItemDeleteFactory;
import mams.command.ListItemPutFactory;


@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class ActiveListItemArtifact extends AbstractActiveItemArtifact {

    {
        requestFactories.put("PUT", new ListItemPutFactory());
        requestFactories.put("DELETE", new ListItemDeleteFactory());
        factories.put("DELETE", new ActiveItemRequestNoBodyCommandFactory());
    }

    

}