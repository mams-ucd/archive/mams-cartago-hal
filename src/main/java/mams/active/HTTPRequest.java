package mams.active;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;


public class HTTPRequest {
    private long id;
    private String method;
    private String type;
    private Object data;
    private ChannelHandlerContext ctx;
    private FullHttpRequest request;
    
    public HTTPRequest(long id, String method, String type, ChannelHandlerContext ctx, FullHttpRequest request) {
           this.method = method;
           this.type = type;
           this.ctx = ctx;
           this.request = request;
	}

    public long getId() {
        return id;
    }

	public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    public void setCtx(ChannelHandlerContext ctx) {
        this.ctx = ctx;
    }

    public FullHttpRequest getRequest() {
        return request;
    }

    public void setRequest(FullHttpRequest request) {
        this.request = request;
    }


}